package labs.apduimpl

import oafc.labs.picc.labs.APDU
import oafc.labs.picc.labs.APDUCipher
import oafc.labs.picc.labs.Constants
import oafc.labs.picc.labs.apduimpl.*
import org.apache.commons.codec.binary.Hex
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory

class APDUTest {

    companion object {
        val logger = LoggerFactory.getLogger(APDUTest::class.java)
    }

    @Test
    fun shouldSelectPICCApplicationLevel() {
        val selectApplication = SelectApplication(byteArrayOf(0x00, 0x00, 0x00))
        val apdu = selectApplication.buildAPDU()
        val expectedAPDU = byteArrayOf(APDU.CLA, Constants.INS_SELECT_APPLICATION, APDU.P1, APDU.P2, 0x03, 0x00, 0x00, 0x00, APDU.LE)
        checkByteArray(apdu, expectedAPDU)
    }

    @Test
    fun shouldSelectApplication() {
        val selectApplication = SelectApplication(byteArrayOf(0x10, 0x20, 0x30))
        val apdu = selectApplication.buildAPDU()
        val expectedAPDU = byteArrayOf(APDU.CLA, Constants.INS_SELECT_APPLICATION, APDU.P1, APDU.P2, 0x03, 0x10, 0x20, 0x30, APDU.LE)
        checkByteArray(apdu, expectedAPDU)
    }

    @Test
    fun shouldRequestISODESChallenge() {
        val requestChallenge = RequestChallenge(KeyType.ISO_DES, 0x00)
        val apdu = requestChallenge.buildAPDU()
        val expectedAPDU = byteArrayOf(APDU.CLA, Constants.INS_AUTH_GET_CHALLENGE_ISO_DES, APDU.P1, APDU.P2, 0x01, 0x00, APDU.LE)
        checkByteArray(apdu, expectedAPDU)
    }

    @Test
    fun shouldRequestAESChallenge() {
        val requestChallenge = RequestChallenge(KeyType.AES, 0x00)
        val apdu = requestChallenge.buildAPDU()
        val expectedAPDU = byteArrayOf(APDU.CLA, Constants.INS_AUTH_GET_CHALLENGE_AES, APDU.P1, APDU.P2, 0x01, 0x00, APDU.LE)
        checkByteArray(apdu, expectedAPDU)
    }

    @Test
    fun shouldAnswerAuthenticationChallenge() {
        val key = ByteArray(24)
        val challenge = Hex.decodeHex("4e34c1314d385402")
        val rndA = Hex.decodeHex("0001020304050607")
        val expectedAPDU = Hex.decodeHex("90af000010c027047c809293e51c6a303514aa66c800")

        authenticate(KeyType.ISO_2KEYS_3DES, key, challenge, rndA, expectedAPDU)
    }

    @Test
    fun shouldGenerateSessionKeyAfterAuthentication() {

        val key = ByteArray(24)
        val challenge = Hex.decodeHex("4e34c1314d385402")
        val rndA = Hex.decodeHex("0001020304050607")
        val expectedAPDU = Hex.decodeHex("90af000010c027047c809293e51c6a303514aa66c800")

        val apduCipher = authenticate(KeyType.ISO_2KEYS_3DES, key, challenge, rndA, expectedAPDU)

        val sessionValidator = SessionValidator()
        val rndAFromPCD = Hex.decodeHex("0001020304050607")
        val rndAFromCard = Hex.decodeHex("b8505207dabb6df9")
        val rndB = Hex.decodeHex("4e34c1314d385402")

        val sessionKey = sessionValidator.generateSessionKey(KeyType.ISO_2KEYS_3DES, apduCipher, rndAFromPCD, rndAFromCard, rndB)
        val expectedSessionKey = Hex.decodeHex("000102034e34c131000102034e34c131000102034e34c131")

        checkByteArray(sessionKey, expectedSessionKey)
    }

    @Test
    fun shouldGenerateSessionKeyAfterAESAuthentication() {

        val key = Hex.decodeHex("00112233445566778899AABBCCDDEEFF")
        val challenge = Hex.decodeHex("DAC90761AAD77F51734E378632ADBB71")
        val rndA = Hex.decodeHex("000102030405060708090A0B0C0D0E0F")
        val expectedAPDU = Hex.decodeHex("90AF00002011BA72713CA96FCA54A6F01093F1694215DD36AAAD129D32CB7181816A14CA1900")

        val apduCipher = authenticate(KeyType.AES, key, challenge, rndA, expectedAPDU)

        val sessionValidator = SessionValidator()
        val rndAFromPCD = Hex.decodeHex("000102030405060708090A0B0C0D0E0F")
        val rndAFromCard = Hex.decodeHex("5375922F31DD18248A5EFEA50D70AB57")
        val rndB = Hex.decodeHex("CD9156D6F3ECE51AECB392E811E309C5")

        val sessionKey = sessionValidator.generateSessionKey(KeyType.AES, apduCipher, rndAFromPCD, rndAFromCard, rndB)
        val expectedSessionKey = Hex.decodeHex("00010203CD9156D60C0D0E0F11E309C5")

        checkByteArray(sessionKey, expectedSessionKey)
    }

    @Test
    fun shouldChangeKey() {

        val sessionKey = Hex.decodeHex("0001020337CAAD460001020337CAAD460001020337CAAD46")
        val newKey = Hex.decodeHex("00112233445566778899AABBCCDDEEFF")
        val keyNo = 128.toByte()
        val keyVersion = 0x00.toByte()
        val apduCipher = APDUCipher(KeyType.ISO_2KEYS_3DES, sessionKey)

        val changeKey = ChangeKey(KeyType.AES, sessionKey, newKey, keyNo, keyVersion, apduCipher)
        val apdu = changeKey.buildAPDU()
        val expectedAPDU = Hex.decodeHex("90C400001980038314C9253922445129B5AE1E18FF78B7394BE6A5DE8B4700")

        checkByteArray(apdu, expectedAPDU)
    }

    @Test
    fun shouldChangeToDESKey() {

        val sessionKey = Hex.decodeHex("00010203CD9156D60C0D0E0F11E309C5")
        val newKey = ByteArray(16)
        val keyNo = 0b00000000.toByte()
        val keyVersion = 0x00.toByte()
        val apduCipher = APDUCipher(KeyType.AES, sessionKey)

        val changeKey = ChangeKey(KeyType.ISO_2KEYS_3DES, sessionKey, newKey, keyNo, keyVersion, apduCipher)
        val apdu = changeKey.buildAPDU()
        val expectedAPDU = Hex.decodeHex("90C400002100F5DA9B9404C03E31123D1016F6FF1DFCEBFDD278329ABA36C07AFFE0AECF58BB00")

        checkByteArray(apdu, expectedAPDU)

    }


    fun authenticate(keyType: KeyType, key: ByteArray, challenge: ByteArray, rndA: ByteArray, expected: ByteArray): APDUCipher {
        // TODO: Create a session/context to share the state between operations

//        val key = ByteArray(24)
//        val challenge = Hex.decodeHex("4e34c1314d385402")
//        val rndA = Hex.decodeHex("0001020304050607")

        val apduCipher = APDUCipher(keyType, key)

        val processChallenge = AuthChallengeAnswer(keyType, key, challenge, rndA, apduCipher)
        val apdu = processChallenge.buildAPDU()

        checkByteArray(apdu, expected)
        return apduCipher
    }


    private fun checkByteArray(actual: ByteArray, expected: ByteArray) {
        logger.trace("Generated: ${Hex.encodeHexString(actual)}, Expected: ${Hex.encodeHexString(expected)}")
        Assertions.assertThat(actual).isEqualTo(expected)
    }

    private fun checkHexString(actual: String, expected: String) {
        logger.trace("Generated: ${actual}, Expected: ${expected}")
        Assertions.assertThat(actual.toUpperCase()).isEqualTo(expected.toUpperCase())
    }


}