package labs.apduimpl

import oafc.labs.picc.labs.APDUCipher
import oafc.labs.picc.labs.apduimpl.*
import org.apache.commons.codec.binary.Hex
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import javax.smartcardio.CardChannel
import javax.smartcardio.TerminalFactory

class AuthenticationIT {

    companion object {
        val logger = LoggerFactory.getLogger(AuthenticationIT::class.java)

        val DEFAULT_AES_KEY = Hex.decodeHex("00112233445566778899AABBCCDDEEFF")
        val DEFAULT_DES = ByteArray(8)
        val DEFAULT_2K3DES_KEY = ByteArray(24)

        private lateinit var channel: CardChannel

        @BeforeAll
        @JvmStatic
        fun initialize() {
            channel = createCardChannel()
        }

        @AfterAll
        @JvmStatic
        fun finalize() {
            logger.info("Closing channel")
            channel.card.disconnect(false)
        }

        private fun createCardChannel(): CardChannel {

            logger.debug("Opening channel...")

            val factory = TerminalFactory.getDefault()
            val terminals = factory.terminals().list()
            logger.debug("Terminals: $terminals")

            val terminal = terminals[0]

            val card = terminal.connect("T=1")

            val atr = card.atr
            logger.debug("ATR: ${Hex.encodeHexString(atr.bytes)}")

            return card.basicChannel
        }

    }

    @Test
    fun shouldAuthenticateWithDESAndGetCardUID() {
        val exchanger = DataExchanger(channel)

        // Request a authentication challenge
        val challenge = exchanger.send(RequestChallenge(KeyType.ISO_DES, 0x00))

        // Perform the authentication with default DES key (0x00, ..., 0x00)
        val rndA = Hex.decodeHex("0001020304050607")
        val apduCipher = APDUCipher(KeyType.ISO_DES, DEFAULT_DES)

        val authChallengeAnswer = AuthChallengeAnswer(KeyType.ISO_DES, DEFAULT_DES, challenge, rndA, apduCipher)
        exchanger.send(authChallengeAnswer)
        exchanger.send(GetCardUID())
    }

    @Test
    fun shouldAuthenticateWithDefaultDESAndChangeToAESKey() {

        val exchanger = DataExchanger(channel)

        // Just select the PICC application (0x00
        exchanger.send(SelectApplication(ByteArray(3)))

        // Request a authentication challenge
        val challenge = exchanger.send(RequestChallenge(KeyType.ISO_DES, 0x00))

        // Perform the authentication with default DES key (0x00, ..., 0x00)
        val rndA = Hex.decodeHex("0001020304050607")
        val apduCipher = APDUCipher(KeyType.ISO_DES, DEFAULT_DES)

        val authChallengeAnswer = AuthChallengeAnswer(KeyType.ISO_DES, DEFAULT_DES, challenge, rndA, apduCipher)
        val rndAFromCard = exchanger.send(authChallengeAnswer)

        // Generate Session Key
        val sessionValidator = SessionValidator()
        val rndAFromPCD = rndA
        val rndB = authChallengeAnswer.rndB

        val sessionKey = sessionValidator.generateSessionKey(KeyType.ISO_DES, apduCipher, rndAFromPCD, rndAFromCard, rndB)
        logger.debug("SessionKey: ${Hex.encodeHexString(sessionKey)}")

        //Change to the AES key
        val newKey = DEFAULT_AES_KEY
        val keyNo = 0b10000000.toByte()
        val keyVersion = 0x00.toByte()
        val newSessionApduCipher = APDUCipher(KeyType.ISO_DES, sessionKey)

        exchanger.send(ChangeKey(KeyType.AES, sessionKey, newKey, keyNo, keyVersion, newSessionApduCipher))

        //Change back to DES Default Key
        shouldAuthenticateWithDefaultAESAndChangeToDES()
    }

    @Test
    fun shouldAuthenticateWithDefault2K3DESAndChangeToAESKey() {

        val exchanger = DataExchanger(channel)

        // Just select the PICC application (0x00
        exchanger.send(SelectApplication(ByteArray(3)))

        // Request a authentication challenge
        val challenge = exchanger.send(RequestChallenge(KeyType.ISO_2KEYS_3DES, 0x00))

        // Perform the authentication with default DES key (0x00, ..., 0x00)
        val rndA = Hex.decodeHex("0001020304050607")
        val apduCipher = APDUCipher(KeyType.ISO_2KEYS_3DES, DEFAULT_2K3DES_KEY)

        val authChallengeAnswer = AuthChallengeAnswer(KeyType.ISO_2KEYS_3DES, DEFAULT_2K3DES_KEY, challenge, rndA, apduCipher)
        val rndAFromCard = exchanger.send(authChallengeAnswer)

        // Generate Session Key
        val sessionValidator = SessionValidator()
        val rndAFromPCD = rndA
        val rndB = authChallengeAnswer.rndB

        val sessionKey = sessionValidator.generateSessionKey(KeyType.ISO_2KEYS_3DES, apduCipher, rndAFromPCD, rndAFromCard, rndB)
        logger.debug("SessionKey: ${Hex.encodeHexString(sessionKey)}")

        //Change to the AES key
        val newKey = DEFAULT_AES_KEY
        val keyNo = 0b10000000.toByte()
        val keyVersion = 0x00.toByte()
        val newSessionApduCipher = APDUCipher(KeyType.ISO_2KEYS_3DES, sessionKey)

        exchanger.send(ChangeKey(KeyType.AES, sessionKey, newKey, keyNo, keyVersion, newSessionApduCipher))

        //Change back to DES Default Key
        shouldAuthenticateWithDefaultAESAndChangeToDES()
    }

    fun shouldAuthenticateWithDefaultAESAndChangeToDES() {

        logger.info("\n\n>>> Change from AES to DES Default KEY <<<")

        val exchanger = DataExchanger(channel)

        // Just select the PICC application (0x00
        exchanger.send(SelectApplication(ByteArray(3)))

        // Request a authentication challenge
        val challenge = exchanger.send(RequestChallenge(KeyType.AES, 0x00))

        // Perform the authentication with default AES key (0x00, ..., 0x00)
        val rndA = Hex.decodeHex("000102030405060708090A0B0C0D0E0F")
        val apduCipher = APDUCipher(KeyType.AES, DEFAULT_AES_KEY)

        val authChallengeAnswer = AuthChallengeAnswer(KeyType.AES, DEFAULT_AES_KEY, challenge, rndA, apduCipher)
        val rndAFromCard = exchanger.send(authChallengeAnswer)

        // Generate Session Key
        val sessionValidator = SessionValidator()
        val rndAFromPCD = rndA
        val rndB = authChallengeAnswer.rndB

        val sessionKey = sessionValidator.generateSessionKey(KeyType.AES, apduCipher, rndAFromPCD, rndAFromCard, rndB)
        logger.debug("SessionKey: ${Hex.encodeHexString(sessionKey)}")

        //Change to the DES key
        val keyNo = 0b00000000.toByte()
        val keyVersion = 0x00.toByte()
        val newSessionApduCipher = APDUCipher(KeyType.AES, sessionKey)

        exchanger.send(ChangeKey(KeyType.ISO_2KEYS_3DES, sessionKey, ByteArray(16), keyNo, keyVersion, newSessionApduCipher))
    }

}

