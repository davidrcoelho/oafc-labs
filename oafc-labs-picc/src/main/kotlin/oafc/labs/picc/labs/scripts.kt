package oafc.labs.picc.labs

import org.apache.commons.codec.binary.Hex
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.security.Provider
import java.security.Security
import java.util.*
import javax.smartcardio.TerminalFactory
import javax.smartcardio.TerminalFactorySpi
import javax.smartcardio.CardTerminals as CardTerminals1


val logger: Logger = LoggerFactory.getLogger("Scripts")
var callMethod = false

fun main() {

    val myProvider = MyProvider()
    Security.addProvider(myProvider)
    System.out.println(Arrays.asList(Security.getProviders()))

    val terminals = TerminalFactory.getInstance("MyType", Any()).terminals().list()
    logger.debug("Terminals: $terminals")

    val terminal = terminals[0]
    val card = terminal.connect("T=1")
    val atr = card.atr
    logger.debug("ATR: ${Hex.encodeHexString(atr.bytes)}")

    if (!callMethod) {
        throw RuntimeException("Expected engineTerminals() not called")
    }

    val factory = TerminalFactory.getDefault()
    val terminals2 = factory.terminals().list()
    logger.debug("Terminals: $terminals2")

}

class MyProvider internal constructor() : Provider("MyProvider", 1.0, "smart Card Example") {

    init {
        put("TerminalFactory.MyType", MyTerminalFactorySpi::class.java.name)
    }
}

class MyTerminalFactorySpi(ob: Any) : TerminalFactorySpi() {

    override fun engineTerminals(): CardTerminals1? {
        println("MyTerminalFactory.engineTerminals() ---")
        callMethod = true
        return null
    }

}