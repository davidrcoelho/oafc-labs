package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDU
import org.apache.commons.codec.binary.Hex
import org.slf4j.LoggerFactory
import javax.smartcardio.CardChannel
import javax.smartcardio.CommandAPDU

class DataExchanger(private var channel: CardChannel) {

    companion object {
        val logger = LoggerFactory.getLogger(DataExchanger::class.java)
    }

    fun send(apdu: APDU): ByteArray {

        val apduData = apdu.buildAPDU()
        logger.debug("Sending APDU: ${apdu.javaClass.simpleName}: ${Hex.encodeHexString(apduData)}")

        val response = channel.transmit(CommandAPDU(apduData))
        logger.debug("${response} ${Hex.encodeHexString(response.data)}")

        if (apdu.validateResponse(response)) {
            logger.debug(":: Success ::")
        } else {
            logger.error(":: Fail ::")
            throw RuntimeException("APDU Response Error.")
        }

        return response.data
    }

}