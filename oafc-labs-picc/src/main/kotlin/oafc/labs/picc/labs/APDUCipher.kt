package oafc.labs.picc.labs

import oafc.labs.picc.labs.apduimpl.KeyType
import org.apache.commons.codec.binary.Hex
import org.slf4j.LoggerFactory
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class APDUCipher {

    private var provider: String
    private var algorithm: String
    private var keyType: KeyType
    private var key: ByteArray
    private var IV: ByteArray

    constructor(keyType: KeyType, key: ByteArray) {
        this.keyType = keyType

        this.provider = "${keyType.algorithm()}/CBC/NoPadding"
        this.algorithm = keyType.algorithm()

        this.key = key
        this.IV = ByteArray(keyType.keySize())
    }

    fun encrypt(data: ByteArray): ByteArray {
        val cipher = getCipher(Cipher.ENCRYPT_MODE)

        val paddedData = this.padding(keyType.keySize(), data)
        logger.trace("Encrypting data. data: ${Hex.encodeHexString(data)}, padded: ${Hex.encodeHexString(paddedData)}")
        val encryptedData = cipher.doFinal(paddedData)

        updateIV(encryptedData)
        return encryptedData
    }

    fun decrypt(data: ByteArray): ByteArray {
        val cipher = getCipher(Cipher.DECRYPT_MODE)
        val decryptedData = cipher.doFinal(data)
        updateIV(data)
        return decryptedData
    }

    private fun padding(paddingSize: Int, data: ByteArray): ByteArray {
        val size = data.size

        val padding: Int
        if (size < paddingSize) {
            padding = paddingSize - size
        } else {
            val remainder = size.rem(paddingSize)
            if (remainder == 0) {
                padding = 0
            } else {
                padding = paddingSize - remainder
            }
        }
        return data + ByteArray(padding)
    }

    private fun updateIV(data: ByteArray) {
        val keySize = this.keyType.keySize()
        val startIndex = data.size - keySize
        logger.trace("updateIV before. IV: ${Hex.encodeHexString(IV)}, data: ${Hex.encodeHexString(data)}, startIndex: $startIndex, endIndex: ${data.size - 1}")
        this.IV = data.sliceArray(startIndex until data.size)
        logger.trace("updateIV after. IV: ${Hex.encodeHexString(IV)}, data: ${Hex.encodeHexString(data)}, startIndex: $startIndex, endIndex: ${data.size - 1}")
    }


    private fun getCipher(mode: Int): Cipher {
        val cipher = Cipher.getInstance(this.provider)
        val keySpec = SecretKeySpec(this.key, this.algorithm)
        val algorithmParamSpec = IvParameterSpec(this.IV)

        logger.trace("getCipher. Algorithm: $algorithm, key: ${Hex.encodeHexString(key)}")
        cipher.init(mode, keySpec, algorithmParamSpec)

        return cipher
    }

    companion object {
        val logger = LoggerFactory.getLogger(APDUCipher::class.java)
    }
}