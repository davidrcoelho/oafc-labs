package oafc.labs.picc.labs

import org.apache.commons.codec.binary.Hex
import java.util.zip.CRC32
import kotlin.experimental.and
import kotlin.experimental.inv

object CRC32Calculator {

    fun calCrc32(data: ByteArray): ByteArray {
        val checksum = CRC32()
        checksum.update(data, 0, data.size)

        val checksumValue = checksum.value

        var hex = java.lang.Long.toHexString(checksumValue).toUpperCase()
        while (hex.length < 8) {
            hex = "0$hex"
        }
        val crc32 = Hex.decodeHex(hex)

        inverseBytes(crc32)
        reverseArray(crc32)
        return crc32
    }

    private fun inverseBytes(data: ByteArray) {
        for (i in data.indices) {
            data[i] = (data[i].inv() and 0xFF.toByte())
        }
    }

    private fun reverseArray(data: ByteArray) {
        for (i in 0 until data.size / 2) {
            val tmp = data[i]
            data[i] = data[data.size - 1 - i]
            data[data.size - 1 - i] = tmp
        }
    }

}
