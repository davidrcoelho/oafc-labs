package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.Constants

enum class KeyType {
    ISO_2KEYS_3DES {
        override fun keySize() = Constants.ISO_3KEY3DES_KEY_SIZE_IN_BYTES
        override fun ins() = Constants.INS_AUTH_GET_CHALLENGE_ISO_DES
        override fun algorithm() = "DESede"
    },
    ISO_DES {
        override fun keySize() = Constants.ISO_3KEY3DES_KEY_SIZE_IN_BYTES
        override fun ins() = Constants.INS_AUTH_GET_CHALLENGE_ISO_DES
        override fun algorithm() = "DES"
    },
    AES {
        override fun keySize() = Constants.AES_KEY_SIZE_IN_BYTES
        override fun ins() = Constants.INS_AUTH_GET_CHALLENGE_AES
        override fun algorithm() = "AES"
    };

    abstract fun ins(): Byte
    abstract fun keySize(): Int
    abstract fun algorithm(): String
}
