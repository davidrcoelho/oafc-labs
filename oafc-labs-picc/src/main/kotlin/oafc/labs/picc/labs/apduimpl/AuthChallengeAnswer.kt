package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDU
import oafc.labs.picc.labs.APDUCipher
import oafc.labs.picc.labs.Constants
import org.apache.commons.codec.binary.Hex
import org.slf4j.LoggerFactory

class AuthChallengeAnswer: APDU {

    var rndB: ByteArray = ByteArray(0)
    // TODO: Create session context to share common information in the same session. For example, sessionKey, rndB, rndA etc

    private val keyType: KeyType
    private var key: ByteArray
    private val challenge: ByteArray
    private val rndA: ByteArray
    private val apduCipher: APDUCipher

    constructor(keyType: KeyType, key: ByteArray, challenge: ByteArray, rndA: ByteArray, apduCipher: APDUCipher) {
        this.keyType = keyType
        this.key = key
        this.challenge = challenge
        this.rndA = rndA
        this.apduCipher = apduCipher
    }

    override fun getCommand(): APDUCommand {
        logger.debug("Processing authentication challenge. keyType: ${keyType}, challenge: ${Hex.encodeHexString(challenge)}, rndA: ${Hex.encodeHexString(rndA)}")

        this.rndB = apduCipher.decrypt(this.challenge)
        logger.debug("rndB: ${Hex.encodeHexString(this.rndB)}")

        val rotatedRndB = rotateLeft(this.rndB)
        logger.debug("rndB (left rotated): ${Hex.encodeHexString(rotatedRndB)}")

        val rndAandRndB = rndA + rotatedRndB
        logger.debug("(rndA + rndB): ${Hex.encodeHexString(rndAandRndB)}")

        val challengeAnswer = apduCipher.encrypt(rndAandRndB)
        logger.debug("Challenge answer enc(rndA + rndB): ${Hex.encodeHexString(challengeAnswer)}")

        return APDUCommand(Constants.INS_ANSWER_AUTH_CHALLENGE, challengeAnswer)
    }

    private fun rotateLeft(data: ByteArray): ByteArray {
        val rotated = ByteArray(data.size)
        rotated[rotated.lastIndex] = data.first()

        for (index in 0 until data.lastIndex) {
            rotated[index] = data[index + 1]
        }
        return rotated
    }

    companion object {
        val logger = LoggerFactory.getLogger(AuthChallengeAnswer::class.java)
    }
}
