package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDU

class RequestChallenge: APDU {

    private var keyType: KeyType
    private var keyNo: Byte

    constructor(keyType: KeyType, keyNo: Byte) {
        this.keyType = keyType
        this.keyNo = keyNo
    }

    override fun getCommand(): APDUCommand {
        return APDUCommand(this.keyType.ins(), byteArrayOf(this.keyNo))
    }

}