package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDU
import oafc.labs.picc.labs.Constants

class GetCardUID: APDU() {

    override fun getCommand(): APDUCommand {
        return APDUCommand(Constants.INS_GET_CARD_UID, ByteArray(0))
    }

}