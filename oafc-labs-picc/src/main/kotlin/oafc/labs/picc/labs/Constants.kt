package oafc.labs.picc.labs

interface Constants {
    companion object {
        const val INS_SELECT_APPLICATION = 0x5A.toByte()
        const val INS_AUTH_GET_CHALLENGE_ISO_DES = 0x1A.toByte()
        const val INS_AUTH_GET_CHALLENGE_AES = 0xAA.toByte()
        const val INS_ANSWER_AUTH_CHALLENGE = 0xAF.toByte()
        const val INS_GET_CARD_UID = 0x51.toByte()
        const val INS_CHANGE_KEY = 0xC4.toByte()

        const val ISO_3KEY3DES_KEY_SIZE_IN_BYTES = 8
        const val AES_KEY_SIZE_IN_BYTES = 16
    }
}