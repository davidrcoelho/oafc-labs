package oafc.labs.picc.labs

import org.apache.commons.codec.binary.Hex
import javax.smartcardio.ResponseAPDU

abstract class APDU {

    data class APDUCommand(val ins: Byte, val data: ByteArray)

    abstract fun getCommand(): APDUCommand

    fun buildAPDU(): ByteArray {
        val (ins, data) = getCommand()
        return wrap(ins, data)
    }

    fun validateResponse(response: ResponseAPDU): Boolean {
        return isSuccess(response)
    }

    protected fun isSuccess(response: ResponseAPDU): Boolean {
        return response.sW1 == DEFAULT_SW1 &&
                (response.sW2 == OPERATION_OK || response.sW2 == ADDITIONAL_DATA)
    }

    private fun wrap(ins: Byte, data: ByteArray): ByteArray {

        val apduSize = when(data.isEmpty()) {
            true -> 5
            false -> 6 + data.size
        }

        val apdu = ByteArray(apduSize)

        apdu[0] = CLA
        apdu[1] = ins
        apdu[2] = P1
        apdu[3] = P2

        if (data.isNotEmpty()) {
            apdu[4] = data.size.toByte()
        }

        for (index in 0..data.lastIndex) {
            apdu[5 + index] = data[index]
        }

        apdu[apdu.lastIndex] = LE

        return apdu
    }

    companion object {
        const val CLA = 0x90.toByte()
        const val P1 = 0x00.toByte()
        const val P2 = 0x00.toByte()
        const val LE = 0x00.toByte()

        const val DEFAULT_SW1 = 0x91
        const val OPERATION_OK = 0x00
        const val ADDITIONAL_DATA = 0xAF
    }

}