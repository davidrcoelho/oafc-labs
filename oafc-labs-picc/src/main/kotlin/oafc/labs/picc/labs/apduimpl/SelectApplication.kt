package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDU
import oafc.labs.picc.labs.Constants

class SelectApplication: APDU {

    private var aid: ByteArray

    constructor(aid: ByteArray) {
        this.aid = aid
    }

    override fun getCommand(): APDUCommand {
        return APDUCommand(Constants.INS_SELECT_APPLICATION, this.aid)
    }
}