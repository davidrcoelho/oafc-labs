package oafc.labs.picc.labs;

import oafc.labs.picc.labs.apduimpl.GetCardUID;
import org.apache.commons.codec.binary.Hex;

import javax.smartcardio.*;
import java.util.List;

public class DESFire {

    public static void main(String[] args) throws CardException {

        TerminalFactory factory = TerminalFactory.getDefault();
        List<CardTerminal> terminalList = factory.terminals().list();

        CardTerminal cardTerminal = terminalList.get(0);
        Card card = cardTerminal.connect("T=1");

        try {

            CardChannel cardChannel = card.getBasicChannel();

            /*
                GetCardUID with no authentication, just to validate that we will get response error.
                When call the GetCardUID without authenticated to the card, the card shall return the error code:
                0xAE which means that the current authentication status does not allow the requested command.
            */
            byte[] getCardUID = new byte[]{(byte)0x90, 0x51, 0x00, 0x00, 0x00};
            System.out.println("> Sending APDU: " + Hex.encodeHexString(getCardUID));
            ResponseAPDU responseAPDU = cardChannel.transmit(new CommandAPDU(getCardUID));
            System.out.println("< " + responseAPDU + Hex.encodeHexString(responseAPDU.getData()));

            // Request Authentication Challenge
            byte[] requestAuthChallenge = new byte[]{(byte)0x90, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x00};
            System.out.println("> Sending APDU: " + Hex.encodeHexString(requestAuthChallenge));
            responseAPDU = cardChannel.transmit(new CommandAPDU(requestAuthChallenge));
            System.out.println("< " + responseAPDU + " - " + Hex.encodeHexString(responseAPDU.getData()));

        } finally {
            card.disconnect(false);
        }


    }

}
