package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDU
import oafc.labs.picc.labs.APDUCipher
import oafc.labs.picc.labs.CRC32Calculator
import oafc.labs.picc.labs.Constants
import org.apache.commons.codec.binary.Hex
import org.slf4j.LoggerFactory
import sun.security.krb5.internal.crypto.crc32

class ChangeKey: APDU {

    private var newKeyType: KeyType
    private val sessionKey: ByteArray
    private val newKey: ByteArray
    private val keyNo: Byte
    private var keyVersion: Byte
    private var apduCipher: APDUCipher

    constructor(newKeyType: KeyType, sessionKey: ByteArray, newKey: ByteArray, keyNo: Byte, keyVersion: Byte, apduCipher: APDUCipher) {
        this.newKeyType = newKeyType
        this.sessionKey = sessionKey
        this.newKey = newKey
        this.keyNo = keyNo
        this.keyVersion = keyVersion
        this.apduCipher = apduCipher
    }

    override fun getCommand(): APDUCommand {

        val newKeyData = when(newKeyType) {
            KeyType.AES -> byteArrayOf(Constants.INS_CHANGE_KEY, keyNo) + newKey + byteArrayOf(keyVersion)
            KeyType.ISO_DES, KeyType.ISO_2KEYS_3DES -> byteArrayOf(Constants.INS_CHANGE_KEY, keyNo) + newKey
        }

        logger.debug("ChangeKey. (0xC4 + keyNo + newKey + [keyVersion]) newKeyData: ${Hex.encodeHexString(newKeyData)} - ${newKeyData.size}")

        val crc32 = CRC32Calculator.calCrc32(newKeyData)

        logger.debug("crc32: ${Hex.encodeHexString(crc32)}")

        val cryptoData = when(newKeyType) {
            KeyType.AES -> newKey + byteArrayOf(keyVersion) + crc32
            KeyType.ISO_DES, KeyType.ISO_2KEYS_3DES -> newKey + crc32
        }
        logger.debug("Crypto data (newKey + keyVersion + crc32): ${Hex.encodeHexString(cryptoData)}")

        val encCommand = apduCipher.encrypt(cryptoData)
        logger.debug("Enc data: ${Hex.encodeHexString(encCommand)}")

        val command = byteArrayOf(keyNo) + encCommand
        logger.debug("Command: ${Hex.encodeHexString(command)}")

        return APDUCommand(Constants.INS_CHANGE_KEY, command)
    }

    companion object {
        val logger = LoggerFactory.getLogger(ChangeKey::class.java)
    }

}