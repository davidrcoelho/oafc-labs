package oafc.labs.picc.labs.apduimpl

import oafc.labs.picc.labs.APDUCipher
import org.apache.commons.codec.binary.Hex
import org.slf4j.LoggerFactory
import java.util.*

class SessionValidator {

    fun generateSessionKey(keyType: KeyType, apduCipher: APDUCipher, rndAFromPCD: ByteArray, encRndAFromCard: ByteArray, rndB: ByteArray): ByteArray {

        logger.debug("Generating SessionKey. keyType: $keyType, rndA: ${Hex.encodeHexString(rndAFromPCD)}, encRndAFromCard: ${Hex.encodeHexString(encRndAFromCard)}, rndB: ${Hex.encodeHexString(rndB)}")

        val rndAFromCard = apduCipher.decrypt(encRndAFromCard)
        logger.debug("rndAFromCard: ${Hex.encodeHexString(rndAFromCard)}")

        rotateRight(rndAFromCard)
        logger.debug("rndAFromCard (right rotated): ${Hex.encodeHexString(rndAFromCard)}")

        if (Arrays.equals(rndAFromPCD, rndAFromCard)) {
            val sessionKey = createSessionKey(keyType, rndAFromPCD, rndB)
            logger.info("Authenticated successfully! SessionKey: ${Hex.encodeHexString(sessionKey)}, rndA: ${Hex.encodeHexString(rndAFromPCD)}, rndB: ${Hex.encodeHexString(rndB)}")
            return sessionKey
        }
        TODO()
    }

    private fun createSessionKey(keyType: KeyType, rndA: ByteArray, rndB: ByteArray): ByteArray {
        when (keyType) {

            KeyType.ISO_DES -> {
                val sessionKey = rndA.slice(0..3) + rndB.slice(0..3)
                return sessionKey.toByteArray()
            }

            KeyType.ISO_2KEYS_3DES -> {
                val sessionKey = rndA.slice(0..3) + rndB.slice(0..3) +
                        rndA.slice(0..3) + rndB.slice(0..3) +
                        rndA.slice(0..3) + rndB.slice(0..3)
                return sessionKey.toByteArray()
            }

            KeyType.AES -> {
                val sessionKey = rndA.slice(0..3) + rndB.slice(0..3) +
                        rndA.slice(12..15) + rndB.slice(12..15)
                return sessionKey.toByteArray()
            }


        }
    }

    private fun rotateRight(data: ByteArray) {
        val last = data.last()
        for (index in data.lastIndex downTo 1) {
            data[index] = data[index - 1]
        }
        data[0] = last
    }


    companion object {
        val logger = LoggerFactory.getLogger(SessionValidator::class.java)
    }

}