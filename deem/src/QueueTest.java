import org.junit.Assert;

public class QueueTest {

    public static void main(String[] args) {
        System.out.println(fibonacci(100));
    }

    static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public void testReversedQueue() {
        // Given a Queue q = 3, 2, 4, 1 , 5
        // Return a reversed queue = 5, 1, 4, 2, 3

        SimpleQueue queue = new MyQueue();

        queue.enQueue(new Integer(3));
        queue.enQueue(new Integer(2));
        queue.enQueue(new Integer(4));
        queue.enQueue(new Integer(1));
        queue.enQueue(new Integer(5));

        QueueManipulator queueManipulator = new QueueManipulator();
        SimpleQueue reversedQueue = queueManipulator.reverseQueue(queue);

        Assert.assertEquals(reversedQueue.deQueue(), new Integer(5));
        Assert.assertEquals(reversedQueue.deQueue(), new Integer(1));
        Assert.assertEquals(reversedQueue.deQueue(), new Integer(4));
        Assert.assertEquals(reversedQueue.deQueue(), new Integer(2));
        Assert.assertEquals(reversedQueue.deQueue(), new Integer(3));
    }

}
