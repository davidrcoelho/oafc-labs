/** Simple Queue: FIFO - First-in First-Out */
public interface SimpleQueue<T> {
    /** @return Removes the first element in Queue and returns it */
    T deQueue();

    /** @param i is placed at the end of the queue*/
    void enQueue(T i);

    boolean isEmpty();

}
