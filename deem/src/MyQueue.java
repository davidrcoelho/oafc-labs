import java.util.ArrayList;

public class MyQueue<T> implements SimpleQueue<T> {
    private ArrayList<T> internalQueue = new ArrayList<T>();
    @Override
    public T deQueue(){
        if(internalQueue.size()==0){
            System.out.println("Queue is empty");
            return null;
        }
        T retVal = internalQueue.remove(0);
        return retVal;
    }
    @Override
    public void enQueue(T i){
        internalQueue.add(i);
    }
    @Override
    public boolean isEmpty() {
        return internalQueue.size()==0;
    }
}
