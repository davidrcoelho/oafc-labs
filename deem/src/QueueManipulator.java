public class QueueManipulator{


    public SimpleQueue reverseQueue(SimpleQueue q) {
        // Given a Queue q = 3, 2, 4, 1 , 5
        // Return a reversed queue = 5, 1, 4, 2, 3

        SimpleQueue reversedQueue = new MyQueue();
        while (!q.isEmpty()) {
            reversedQueue.enQueue(q.deQueue());
        }

        return reversedQueue;
    }
}
